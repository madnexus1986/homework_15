// homework 15.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>


void print_number(int PrintType, int N)
{

        for (int i = PrintType; i < N + 1; i = i + 2)
        {
                std::cout << i << "\n";
        }
}


int main()
{
    int N;
    int PrintType;
    std::cout << "Enter N" << "\n";
    std::cin >> N;
    std::cout << "\n";
    for (int i = 0; i < N+1; ++i)
    {
        if (i % 2 == 0)
        {
            std::cout << i << "\n";
        }
    }
    std::cout << "Enter print type : 1 - odd, 0 - even" << "\n";
    std::cin >> PrintType;
    std::cout << "\n";
    print_number(PrintType, N);


}

